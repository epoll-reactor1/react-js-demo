import Slider from './../../slider/Index';
import '../Style.css';

const TabSound = () =>
  <div className='tab-contents'>
    <Slider name={"Sound"} />
    <Slider name={"Brightness"} />
    <Slider name={"Notifications"} />
  </div>

export default TabSound