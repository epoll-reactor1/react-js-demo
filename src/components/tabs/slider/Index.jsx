import React from "react";

const Slider = ({name}) =>
  <>
    <div class="section-name">{name}</div>
    <div class="slider-container">
      <input type="range" min="0" max="100" class="volume-slider" />
    </div>
  </>

export default Slider