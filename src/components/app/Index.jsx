import React, { useState } from 'react';
import TopBar from '../topBar/Index';
import MainWindow from '../mainWindow/Index';

const App = () => {
  const [activeTab, setActiveTab] = useState('Network');

  const handleTabChange = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div>
      <TopBar onTabChange={handleTabChange} />
      <MainWindow activeTab={activeTab} />
    </div>
  );
}

export default App