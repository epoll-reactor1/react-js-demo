import React from 'react';
import TabNetwork from '../tabs/network/Index';
import TabSound from '../tabs/sound/tab/Index';

const MainWindow = ({ activeTab }) =>
  <div>
    {activeTab === 'Network' && <TabNetwork />}
    {activeTab === 'Sound' && <TabSound />}
  </div>

export default MainWindow