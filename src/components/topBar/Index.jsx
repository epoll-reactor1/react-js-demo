import React from 'react';
import './Style.css';

const TopBar = ({ onTabChange }) => {
  const handleClick = (tab) => {
    onTabChange(tab);
  };

  return (
    <div>
      <div class='container'>
        <tl class='menu-items'>
          <th onClick={() => handleClick('Network')}>Network</th>
          <th onClick={() => handleClick('Sound')}>Sound</th>
        </tl>
      </div>
    </div>
  );
};

export default TopBar